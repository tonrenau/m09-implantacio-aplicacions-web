<?php
require_once('address.php');

class Customer
{
    // Properties
    private int $customer_id;
    private string $first_name;
    private string $last_name;
    private string $email;
    private array $addresses;
}
