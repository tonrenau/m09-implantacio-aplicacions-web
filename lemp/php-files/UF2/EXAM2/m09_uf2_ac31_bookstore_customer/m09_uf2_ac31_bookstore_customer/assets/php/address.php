<?php
require_once('country.php');

class Address
{
    // Properties
    private int $address_id;
    private string $street_number;
    private string $street_name;
    private string $city;
    private Country $country;
    private string $status;

    //constructor
    public function __construct()
    {
        $this->address_id = 0;
    }

}
