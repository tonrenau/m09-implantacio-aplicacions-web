<?php

class Country
{
    // Properties
    private int $country_id;
    private string $country_name;

    //constructor 
    public function __construct( $country_id, $country_name)
    {
      $this->country_id = $country_id;
      $this->country_name = $country_name;
    }

    //getters 
    public function getCountryId()
    {
      return $this->country_id;
    }

    public function getCountryName()
    {
      return $this->country_name;
    }

    //toStr
    public function __toString()
    {
      return "id: ".$this->country_id." of ".$this->country_name;
    } 

  }
