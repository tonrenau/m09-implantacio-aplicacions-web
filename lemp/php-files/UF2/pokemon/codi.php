<?php

require_once 'pokemon.php';
require_once 'pokemons.php';

$url = "https://joanseculi.com/json/pokemons.json";
$json_data = file_get_contents($url);
$data = json_decode($json_data, true);

//

function validate($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$pokemons = new Pokemons();

foreach ($data['pokemons'] as $pokemon_info) {

    $pokemon = new Pokemon(
        $pokemon_info['Code'],
        $pokemon_info['Name'],
        $pokemon_info['Type1'],
        $pokemon_info['Type2'],
        $pokemon_info['HealthPoints'],
        $pokemon_info['Attack'],
        $pokemon_info['Defense'],
        $pokemon_info['SpecialAttack'],
        $pokemon_info['SpecialDefense'],
        $pokemon_info['Speed'],
        $pokemon_info['Generation'],
        $pokemon_info['Legendary'],
        $pokemon_info['Image']
    );
    $pokemons->add_pokemon($pokemon);
}

echo "<table>";
    // select generacio pokemon
    echo "<form method='POST' action='" . validate($_SERVER['PHP_SELF']) . "'>";
        echo "<tr>";
            echo "<td> Generation </td>";
            echo "<td>";
                echo "<div class='row'>";
                    echo "<div class='col-md-6'>";
                        echo "<select name='generacio' class='form-control'>";
                            echo "<option value='All'> All </option>";
                                for ($i=1; $i <= 6; $i++) { 
                                    $selected = isset($_POST['generacio']) && $_POST['generacio'] == $i ? 'selected' : ''; 
                                    echo "<option value='$i' $selected> $i </option>";
                                }
                        echo "</select>";
                    echo "</div>";
                    echo "<div class='col-md-3'>";
                        echo "<button class='btn btn-primary' type='submit' name='execute'>Search</button>";
                    echo "</div>";
                echo "</div>";
            echo "</td>";
        echo "</tr>";
        // SEARCH POKEMON
        echo "<tr>";
            echo "<td>Search Pokemons: </td>";
                echo "<td>";
                    echo "<div class='row'>";
                        echo "<div class='col-md-6'>";
                            echo "<input type='text' name='pokemon_name' class='form-control' size='60' value='" . 
                            (isset($_POST['pokemon_name']) ? $_POST['pokemon_name'] : '') . "'>"; 
                        echo "</div>";
                    echo "<div class='col-md-6'>";
                        echo "<button class='btn btn-primary' type='submit' name='execute_name'>Search</button>";
                    echo "</div>";
                echo "</td>";
            echo "<td>";
        echo "</tr>";
        // SEARCH TYPE
        echo "<tr>";
            echo "<td>Search Type: </td>";
                echo "<td>";
                    echo "<div class='row'>";
                        echo "<div class='col-md-6'>";
                            echo "<input type='text' name='pokemon_type' class='form-control' size='60' value='" . 
                            (isset($_POST['pokemon_type']) ? $_POST['pokemon_type'] : '') . "'>"; 
                        echo "</div>";
                    echo "<div class='col-md-6'>";
                        echo "<button class='btn btn-primary' type='submit' name='execute_type'>Search</button>";
                    echo "</div>";
                echo "</td>";
            echo "<td>";
        echo "</tr>";
    echo "</form>";
    // MOSTRATGE POKEMONS
    echo "<br>";

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['execute'])) {
            generacio($pokemons);
        }
        if (isset($_POST['execute_name'])) {
            nom($pokemons);
        }
        if (isset($_POST['execute_type'])) {
            tipus($pokemons);
        }
    }
    
    function generacio($pokemons) {
        $select_generation = $_POST["generacio"];
        if ($select_generation == "All") {
            $filter_pokemons = $pokemons->get_pokemons();
        } else {
            $filter_pokemons = $pokemons->get_generation($select_generation);
        }

        pokemons_table($filter_pokemons);
    }

    function nom($pokemons) {
        $nom = $_POST["pokemon_name"];
        $filter_pokemons = $pokemons->get_pokemon_name($nom);
        pokemons_table($filter_pokemons);
    }

    function tipus($pokemons) {
        $tipus = $_POST["pokemon_type"];
        $filter_pokemons = $pokemons->get_pokemon_type($tipus);
        pokemons_table($filter_pokemons);
    }

    function pokemons_table($filter_pokemons){
        echo "<table class='table table-striped table-primary'>";
        echo "<br>";
        echo "<thead class='table-success'>";
        echo "<tr>";
        echo "<th>Image</th>";
        echo "<th>Code</th>";
        echo "<th>Name</th>";
        echo "<th>Type1</th>";
        echo "<th>Type2</th>";
        echo "<th>HP</th>";
        echo "<th>ATK</th>";
        echo "<th>DEF</th>";
        echo "<th>SP ATK</th>";
        echo "<th>SP DEF</th>";
        echo "<th>SPD</th>";
        echo "<th>GEN</th>";
        echo "<th>LEG</th>";
        echo "<th>TOTAL</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody'>";

            foreach ($filter_pokemons as $pokemon){
                echo "<tr>";
                echo "<td><img src='{$pokemon->get_image()}' alt='{$pokemon->get_name()}' width='200'></td>";
                echo "<td>{$pokemon->get_code()}</td>";
                echo "<td>{$pokemon->get_name()}</td>";
                echo "<td>{$pokemon->get_type1()}</td>";
                echo "<td>{$pokemon->get_type2()}</td>";
                echo "<td>{$pokemon->get_healthPoints()}</td>";
                echo "<td>{$pokemon->get_attack()}</td>";
                echo "<td>{$pokemon->get_defense()}</td>";
                echo "<td>{$pokemon->get_specialAttack()}</td>";
                echo "<td>{$pokemon->get_specialDefense()}</td>";
                echo "<td>{$pokemon->get_speed()}</td>";
                echo "<td>{$pokemon->get_generation()}</td>";
                echo "<td>".($pokemon->get_legendary() ? 'Yes' : 'No') . "</td>";
                echo "<td>{$pokemon->total()}</td>";
                echo "</tr>";
            }
        echo "</tbody>";
        echo "</table>";
    }

echo "</table>";




?>
