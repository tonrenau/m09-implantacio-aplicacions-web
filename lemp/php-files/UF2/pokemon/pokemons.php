<?php
require_once('pokemon.php');

// Atributs
class Pokemons {
    private array $pokemons;
    
    //construct 
    public function __construct() {
        $this->pokemons = array();              
    }

    //gets i sets
    public function get_pokemons(): array {
        return $this->pokemons;
    }
    public function set_pokemons(array $value): void {
        $this->pokemons[] = $value;
    }

    // metodes 
    public function add_pokemon(Pokemon $pokemon) { 
        $this->pokemons[] = $pokemon;
    }


    //function que retorna la llista de pokemons de una gen en concret
    public function get_generation(int $generation) {
        $array_gen_pokemon = array();
        foreach($this->get_pokemons()as $poke) {
            if ($poke->get_generation() == $generation) {
                $array_gen_pokemon[] = $poke;
            }
        }
        return $array_gen_pokemon;
    }

    //get_pokemon name 
    public function get_pokemon_name(string $name)
    {
            $name = strtolower($name);
            $array_pokemon_name = array();

            foreach($this->get_pokemons() as $poke) 
            {
                if (str_contains(strtolower($poke->get_name()), $name)) 
                {
                    $array_pokemon_name[] = $poke;
                }
            }
        return $array_pokemon_name;
    }


    //get_pok_type retorn llista de pokemons amb el type1 o type2

    public function get_pokemon_type(string $type) {
        $type = strtolower($type);
        $array_pok_type = array();

        foreach($this->get_pokemons() as $poke){
            if (strpos(strtolower($poke->get_type1()), $type) !== false || strpos(strtolower($poke->get_type2()), $type) !== false) {
                $array_pok_type[] = $poke;
            }
        }
        return $array_pok_type;
    }   

}
?>

