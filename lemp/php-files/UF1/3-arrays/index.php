<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Arrays</title>
    <link rel="stylesheet" href="style.css">

    <style>

    table {
        border-collapse: collapse;
        width: 90%;
        margin: auto;
    }
    .table_aleatori{
        background-color: lightblue;
    }
    .par_impar_table{
        background-color: lightgreen;
    }
    .mult_ten {
        background-color: lightgray;
    }

    table, th, td {
        border: 1px solid black;
    }

    </style>
</head>
<body>
    <?php include "app.php" ?>
</body>
</html>