<?php
    echo "<h1>1. Array aleatori</h1>";
    $len = 20;
    $random_array = array();

    for ($i = 1; $i <= $len ; $i++) {
        $random_array[$i] = rand(100,999);
    }

    echo "<table class='table_aleatori'>";
        echo "<tr>";
            for ($i=1; $i <= count($random_array) ; $i++) { 
                echo "<th>$i</th>";
            }
        echo "</tr>";

        echo "<tr>";
            for ($i=1; $i <= count($random_array) ; $i++) { 
                echo "<td>$random_array[$i]</td>";
            } 
        echo "</tr>";
    echo "</table>";
    
?>

<?php 
    echo "<h1>2. Array parells i imparells</h1>";
    $len = 10;
    $array = array();
    $par_suma = 0;
    $num_par = 0;
    $impar_suma = 0;
    $num_impar = 0;
    for ($i=1; $i <= $len ; $i++) { 
        $array[$i] = rand(100,999);
        if ($array[$i] % 2 == 0) {
            $par_suma += $array[$i];
            $num_par += 1;
        } else {
            $impar_suma += $array[$i];
            $num_impar += 1;
        }      
    }

    echo "<table class='par_impar_table'>";
        echo "<tr>";
            for ($i=1; $i <= count($array) ; $i++) { 
                echo "<th>$i</th>";
            }
        echo "</tr>";
        echo "<tr>";
            for ($i=1; $i <= count($array) ; $i++) { 
                echo "<td>$array[$i]</td>";
            }
        echo "</tr>";
    echo "</table>";

    echo "<h1>3. Total pars i impars </h1>";
    $total = $par_suma + $impar_suma;
    echo "<table>";
            echo "<tr>";
                echo "<p>Total suma de $num_par/$len numeros pars: </p>" ;
                echo $par_suma;
            echo "</tr>";
            echo "<tr>";
                echo "<p>Total suma de $num_impar/$len numeros impars: </p>" ;
                echo $impar_suma;
            echo "</tr>";
            echo "<tr>";
                echo "<p>Suma total: </p>";
                echo $total;
            echo "</tr>";
    echo "</table>";
    $avg = ($total / 2);
    echo "<h1>4. Average </h1>";
    echo "<table>";
            echo "<tr>";
                echo "<p>Avg: $avg </p>";
            echo "</tr>";
    echo "</table>";
?>



<?php
    echo "<h1>5. Array multiple de 10 </h1>";
    $len = 3; 
    $mult_array = array();
    for ($i=1; $i <= $len ; $i++) { 
        $num_aletori = rand(10,99);
        $mult_array[$i] = $num_aletori * 10;
    }
    echo "<table class='mult_ten'>";
        echo "<tr>";
            for ($i=1; $i <= count($mult_array) ; $i++) { 
                echo "<th>$i</th>";
            }
        echo "</tr>";
        echo "<tr>";
            for ($i=1; $i <= count($mult_array) ; $i++) { 
                    echo "<td>$mult_array[$i]</td>";
            }
        echo "</tr>";
    echo "</table>";
?>



<?php 
    echo "<h1>6. Array mes gran i mes petit de average </h1>";
    $len = 10;
    $array = array();
    $par_suma = 0;
    $num_par = 0;
    $impar_suma = 0;
    $num_impar = 0;
    for ($i=1; $i <= $len ; $i++) { 
        $array[$i] = rand(100,999);
        if ($array[$i] % 2 == 0) {
            $par_suma += $array[$i];
            $num_par += 1;
        } else {
            $impar_suma += $array[$i];
            $num_impar += 1;
        }      
    }
    $total = $par_suma + $impar_suma;
    $avg = $total / 2;
    echo "<table>";
        echo "<p>Taula que te un average mes petit a: $avg</p>";
        echo "<tr>";
            for ($i=1; $i <= count($array) ; $i++) { 
                echo "<th>$i</th>";
            }
        echo "</tr>";
        echo "<tr>";
            for ($i=1; $i <= count($array) ; $i++) { 
                echo "<td>$array[$i]</td>";
            }
        echo "</tr>";
    echo "</table>";
?>