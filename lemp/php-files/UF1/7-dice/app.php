<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form method="post">
        <p>Number: </p>
        <input type="text" name='num' id='num' min="1" max="6" requered>
        <br/><br/>
        <input type="submit" name='submit' value='ROLL'>
        <p>Click de button to roll a number of dice</p>
    </form>
    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $dice = intval($_POST['num']);
            echo "<h2>Rolling " .$dice. " dice</h2>";
        }

        $values = array();
        $sum = 0;
        $images = array(
            1 => "./images/img1.png",
            2 => "./images/img2.png",
            3 => "./images/img3.jpeg",
            4 => "./images/img4.png",
            5 => "./images/img5.png",
            6 => "./images/img6.jpeg",
        );
        for ($i=1; $i <= $dice ; $i++) { 
            $random_num = rand(1,6);
            $values[] = $random_num;
            $sum += $random_num;
            echo "<img src='{$images[$random_num]}' alt='img $random_num'> ";
        }

        echo "<h2>Result</h2>";
        echo "<p>The values are: " . implode(", ", $values) . "</p>";
        echo "<p>Total: $sum </p>";
    ?>
</body>
</html>