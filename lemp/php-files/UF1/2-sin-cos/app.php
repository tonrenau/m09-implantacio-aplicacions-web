<?php
echo "<table>";
    echo "<thead>";
    echo "<tr >";
        echo "<th id='head'>Degree</th>";
        echo "<th id='head'>Radians</th>";
        echo "<th id='head'>Sine</th>";
        echo "<th id='head'>cosine</th>";
    echo "</tr>";
    echo "</thead>";
for ($degree = 0; $degree <= 360; $degree++) {
    $radians = round(deg2rad($degree),4);
    $sine = round(sin($radians),4);
    $cosine = round(cos($radians),4);

    echo "<tr>";
    echo "<td>" . $degree . "</td>";
    echo "<td>" . $radians . "</td>";

    if ($sine < 0){
        echo "<td class='negative'>" . $sine . "</td>";
    }else{
        echo "<td class='postive'>" . $sine . "</td>";
    }

    if ($cosine < 0){
        echo "<td class='negative'>" . $cosine . "</td>";
    }else{
        echo "<td class='postive'>" . $cosine . "</td>";
    }

    echo "</tr>";
}


echo "</table>";
?>