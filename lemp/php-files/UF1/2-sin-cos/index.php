<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link type="text/css" rel="stylesheet" href="style.css">
    <title>Sin Cos</title>

    <style>
    p { 
    text-align: center;
    margin: auto;
    }

    table {
        border-collapse: collapse;
        width: 80%;
        margin: auto;
    }

    table, th, td {
        border: 1px solid black;
    }
    td.negative {
        color: red;
    }
    td {
        color: blue;
    }

    </style>
</head>
<body>
    <h1>Sine And Cosine</h1>
    <p><img src="image.jpg" alt="image" ></p>

    <div >
            <?php include "app.php" ?>
    </div>

</body>
</html>