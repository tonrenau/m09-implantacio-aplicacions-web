<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Exponent Calculator</h2>
    <form method="post">
        <p>x**n</p>
        <p>x= <input type='text' name='x' id='x' required></p>
        <p>n= <input type='text' name='n' id='n' required></p>
        <br/>
        <input type='submit' name='submit' value='Calulate'>
    </form>
    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $base = floatval($_POST['x']);
            $expo = floatval($_POST['n']);  

            if (is_numeric($base) && is_numeric($expo)) {
                if ($expo != intval($expo)) {
                    $result = pow($base, $expo);
                } else {
                    $result = 1;
                    if ($expo > 0) {
                        for ($i = 0; $i < $expo; $i++) {
                            $result *= $base;
                        }
                    } elseif ($expo < 0) {
                        $expo = abs($expo);
                        for ($i = 0; $i < $expo; $i++) {
                            $result *= (1 / $base);
                        }
                    }
                }
            }
            echo "<p>Exponen calculculator: </p>";
            echo "<p>x**n = " . $result . "</p>";
        }
            
    ?>
    <h4>Positive Exponent</h4>
    <p>
    Exponentiation is a mathematical operation, written as xn, involving the base x and an 
    exponent n. In the case where n is a positive integer, exponentiation corresponds to 
    repeated multiplication of the base, n times.

    The calculator above accepts negative bases, but does not compute imaginary numbers. It 
    also does not accept fractions, but can be used to compute fractional exponents, as long 
    as the exponents are input in their decimal form.
    </p>
    <h4>Negative Exponent</h4>
    <p>
    When an exponent is negative, the negative sign is removed by reciprocating the base and 
    raising it to the positive exponent.
    So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
    until n is equal to 0.
    </p>
    <h4>Exponent 0</h4>
    <p>
    When an exponent is 0, any number raised to the 0 power is 1.
    For 0 raised to the 0 power the answer is 1 however this is considered a definition and not 
    an actual calculation.
    </p>
    <h4>Real Number exponent</h4>
    <p>For real numbers, we use the PHP function pow(n,x)</p>
</body>
</html>