<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        echo "<h1>Animes</h1>";

        $url = "https://joanseculi.com/edt69/animes3.php" ; // path to your JSON file
        $data = file_get_contents($url); // put the contents of the file into a variable
        $obj = json_decode($data, true); // decode the JSON feed

        $array_id = array();
        $array_genre = array();
        $array_years = array();
        
        
        foreach ($obj["animes"] as $value) {
            $array_id = $value['id'];
            $array_genre = $value['genre'];
            $array_years = $value['year'];
        }

        echo "<h4>Num animes: $array_id </h4>";
        echo "<h4>Generes: $array_genre</h4>";
        echo "<h4>Years: $array_years </h4>";

        foreach ($obj["animes"] as $value) {
            echo "<table>";
                echo "<img src='https://joanseculi.com/{$value['image']}' width=20%>";
                echo "<tr>";
                    echo "<td>ID: ". $value['id'] ."</td>";
                    echo "<td>TYPE: ". $value['type'] ."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>NAME: ". $value['name'] ."</td>";
                    echo "<td>YEAR: ". $value['year'] ."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>Original Name: ". $value['originalname'] ."</td>";
                    echo "<td>RATING: ". $value['rating'] ."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>DEMOGRAPHY: ". $value['demography'] ."</td>";
                    echo "<td>GENRE: ". $value['genre'] ."</td>";
                echo "</tr>";
                echo "<tr>";
                    echo "<td>SYNOPSIS: ". $value['description'] ."</td>";
                echo "</tr>";
            echo "</table>";
            
        }
        
        
    
    ?>
</body>
</html>


