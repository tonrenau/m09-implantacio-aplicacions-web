<?php
    $sell_console = array ( 
        "Ps4" => 102,
        "Wii" => 101,
        "Ps2" => 1555,
        "Nintendo DS" => 154,
        "Game Boy" => 119,
        "Ps3" => 87,
        "Xbox 360" => 84,
        "PSP" => 82,
        "Game Boy Advance" => 81,
    );

    // ordenar array segons num vendes
   arsort($sell_console);

    echo "<table>";
     foreach ($sell_console as $key => $value) {
        echo "<tr>";
        echo "<td>" . $key . ":" . "</td>";
        $repit = $value * 100/1555;
        
        echo "<td>";
        $bar_lenght = min($value * 2, round($repit)); 
        echo str_repeat("<img src='green.png'>", $bar_lenght);
    
        echo "  ". $value . " Millions"  . "</td>" ;
        echo "</tr>";
    }
    echo "</table>";
   


?>