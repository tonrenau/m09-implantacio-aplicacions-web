<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Factorial Calculator: n!</h1>
    <div>
        <form method="post">
            <p>Enter a number to calculate the factorial n! using the calculator below.</p>
            <p>Number: <input type='text' name='num' id='num' required></input></p>
            <br/>
            <input type='submit' name='submit' value='SEND'>
        </form>

        <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $num = ($_POST['num']);
                $factorial = 1;
                for ($i = 1; $i <= $num; $i++) {
                    $factorial *= $i;
                }
            }
               
            echo "<p>Factorial Number:</p>";
            echo "<p>n! = " . $factorial . "</p>";
        ?>
        
    </div>
    
    <h3>Factorial Formulas </h3>
    <p>
        The formula to calculate a factorial for a number is:
        n! = n × (n-1) × (n-2) × ... × 1
        Thus, the factorial n! is equal to n times n minus 1, times n minus 2, continuing until the 
        number 1 is reached.
        The factorial of zero is 1: 
        0! = 1
    </p>
    <h3>Recurrence Relation</h3>
    <p>
    And the formula expressed using the recurrence relation looks like this: 
    n! = n × (n – 1)!                                                                   
    So the factorial n! is equal to the number n times the factorial of n minus one. This recurses
    until n is equal to 0. 
    </p>
    <h3>Factorial Table</h3>
    <p>
    The table below shows the factorial n! for the numbers one to one-hundred. 
    </p>

    <?php
    echo "<table>";
        echo "<tr>";
            echo "<th> n </th>";
            echo "<th> n! </th>";
       echo "</tr>";

       for ($i=0; $i <= 100 ; $i++) {
            echo "<tr>";
                echo "<td>$i</td>";

                echo "<td>";
                    if ($i == 0 || $i == 1) {
                        $factorial = 1;
                    } else {
                    $factorial *= $i ;
                    }
                    echo $factorial;
                echo "</td>";
            echo "</tr>";
    }
    echo "</table>";
    ?>
    

</body>
</html>

