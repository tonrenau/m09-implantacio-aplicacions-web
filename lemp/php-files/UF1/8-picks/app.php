<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $url = 'https://api.jsonserve.com/Gk1Go7';
        $data = file_get_contents($url);
        $array = json_decode($data, true);

        foreach ($array as $item){
            echo "<img src={$item['url']} alt='img'>";
            echo "Name:  " . $item['name'] . "<br/>";
            echo "Height: ". $item['height']. "<br/>";
            echo "Prominence: ". $item['prominence']. "<br/>";
            echo "Zone: ". $item['zone']. "<br/>";
            echo "Country: ". $item['country']. "<br/> <br/>";
        }
    ?>
</body>
</html>