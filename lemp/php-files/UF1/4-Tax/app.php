<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>app</title>
</head>
<body>
    <h1>PRICE, TAX and ROUNDS</h1>

    <form method="post"> <!-- Agregar el metode del form, en aquest cas POST -->
        <p>price with tax: </p>
        <input type='text' name='price' id='price' required > <!--  posar required perque el camp sigui obligatori.-->
        <p>TAX(%):</p>
        <input type='text' name='tax' id='tax' required><br><br>
        <input type='submit' name='submit' value='Calculate'>
    </form>

    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") { // verficar que s'ha enviat el formulari
            $price = floatval($_POST['price']);//floatval es pq sigi numeric
            $tax = floatval($_POST['tax']);

            $desc = ($price * $tax)/100;
            $fabric_price = $price - $desc; 

            echo "<h3>TAX data:</h3>";
            echo "<p>Price without tax: " . $fabric_price . " euros. </p>";
            echo "<p>Round to 2 decimals using round(): " . round($fabric_price, 2) . " euros. </p>";
            echo "<p>Round to 4 decimals using sprintf(): " . sprintf("%.4f", $fabric_price) . " euros. </p>";
    }

        
    ?>



</body>
</html>